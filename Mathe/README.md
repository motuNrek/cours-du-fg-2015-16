# Cours de maths #

Le cours que je propose ici repose sur le cours de mathématiques de M. Brévart. 
Le but de taper le cours est faciliter à tout le monde le travail de révision avant des DS ou des Klausuren.
Si vous trouvez des fautes, je vous serais reconnaissant de m'indiquer la page, le paragraphe et la faute. Merci!

## Version Beta ##

Les documents qui ne sont pas encore corrigés par M. Brévart sont indiqué par "version_beta". Il sont disponible sujet par sujet,
mais également en cours continu. Dès que M. Brévart trouve le temps de relire le cours et de m'indiquer toutes les fautes mathématiques 
graves, je les corrigerai et proposerai un document qui contient les cours corrigés jusqu'alors.


## en construction ##

Je commence déjà à taper le cours, même si nous n'avons pas encore fini un chapitre. Ces document sont marqué par "en_construction". Cela permet de réviser 
déjà la première partie du cours.
