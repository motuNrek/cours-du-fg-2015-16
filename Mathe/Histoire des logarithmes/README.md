# L'histoire des logarithmes #

## Pourquoi ce dossier? ##

Je trouve que la lecture de ces documents est important pour deux raison:

	1. Il est très intéressant comment on a réellement construit le logarithme (il s'agit d'une toute autre construction que celle que nous apprenons au lycée)

	2. Si on connaît l'histoire des logarithmes, on comprend mieux pourquoi le logarithme et aussi important que les profs nous le présentent. Le cours devient moins abstrait et par cela 
peut-être même plus intéressant.

## Qu'apportent ces documents? ##

Les deux documents présentent la "découverte" des logarithmes par Neper à partir de deux points de vue: L'un explique plus les causes, l'histoire et les idées principales de Neper, l'autre
est plus technique et montre le raisonnement géométrique en illustrant les étapes. Cependant, on comprend beaucoup mieux le premier article, d'après moi. Je vous propose donc les deux articles
qui sont complémentaires.
