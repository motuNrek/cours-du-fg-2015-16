## Cours de svt ##

Voici une version digitalisée du cours de SVT, du tronc commun et de la spécialité, afin que la révision des devoirs et Klausuren soit facilitée. 
Je diviserai le cours selon les chapitres (Géologie, Géothermie, Génétique, ...), en y ajoutant aussi des documents de méthodologie.

Toutefois, je m'appuierai, pour le cours du tronc commun, sur le cours de Mme Moreau - il se peut donc que les élèves de Mme Blondeau aient des activités (et des titres) différentes.

Comme pour le cours de mathématiques, ce serait bien que vous me communiquiez les fautes trouvées
J'essaierai d'améliorer la qualité au fur et à mesure que je digitalise le cours, mais je suis ouvert aux remarques
- Merci à l'avance!

## Cours ##

Les documents nommées " - cours " sont uniquement des versions tapées des Bilans des activités (en noir) et des notions clés du programme (rouge).
Éventuellement, je rajouterais les schémas importants réalisées

## COMPLET ##

Dans ces fichiers, je rajouterais plus ou moins tout ce que nous recevons comme documents: les activités (scannées toutefois, donc parfois en qualité inférieure), et ce que Mme Moreau rajoute sur l'ENT.
Ces fichiers seront mises à jour plus souvent, mais la mise à jour peut parfois se retarder en raison des activités pas encore corrigées, etc.
J'y rajoute aussi des notes personelles, mais c'est juste pour faire le point sur les activités et notions (isostasie, etc.); toutefois, ce ne seront pas des indormations supplémentaires.
