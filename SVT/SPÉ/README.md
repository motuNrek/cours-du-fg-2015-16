## Cours de SPÉ ##

Ici se trouvent les documents du cours de SPÉ - le plan du cours est orienté plutôt vers celui de Mme Blondeau. Si je confonds bilan de TP et Bilan du programme, je suis ouvert aux corrections.

## TP type ECE ##

J'essaierai de mettre tous les TP réalisés sous format ECE avec ma fiche méthodologique; certains schémas sont dessinées, mais j'essaierai de les refaire au fur et à mesure.
